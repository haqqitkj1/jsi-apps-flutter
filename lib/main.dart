import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:jsi_apps/cubit/page_cubit.dart';
import 'package:jsi_apps/pages/login_page.dart';
import 'package:jsi_apps/pages/login_page_filkom.dart';
import 'package:jsi_apps/pages/login_page_one.dart';
import 'package:jsi_apps/pages/main_page.dart';
import 'package:jsi_apps/pages/menu_page.dart';
import 'package:jsi_apps/pages/news_page.dart';
import 'package:jsi_apps/pages/onboardscreen.dart';
import 'package:jsi_apps/pages/presensi_rapat_esign_one.dart';
import 'package:jsi_apps/pages/presensi_rapat_esign_two.dart';
import 'package:jsi_apps/pages/presensi_rapat_one.dart';
import 'package:jsi_apps/pages/presensi_rapat_two.dart';
import 'package:jsi_apps/pages/profil_page.dart';
import 'package:jsi_apps/pages/rapat_detil.dart';
import 'package:jsi_apps/pages/signature_page.dart';
import 'package:jsi_apps/pages/splashscreen.dart';
import 'package:jsi_apps/providers/auth_provider.dart';
import 'package:jsi_apps/providers/news_latest_provider.dart';
import 'package:jsi_apps/providers/news_provider.dart';
import 'package:jsi_apps/providers/rapat_presensi_provider.dart';
import 'package:jsi_apps/providers/rapat_provider.dart';
import 'package:jsi_apps/providers/user_provider.dart';
import 'package:provider/provider.dart';
// import 'package:jsi_apps/pages/onboardscreen.dart';
// import 'package:jsi_apps/pages/splashscreen.dart';

// void main() {
//   runApp(MyApp());
// }

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (context) => AuthProvider(),
        ),
        ChangeNotifierProvider(
          create: (context) => UserProvider(),
        ),
        ChangeNotifierProvider(
          create: (context) => RapatProvider(),
        ),
        ChangeNotifierProvider(
          create: (context) => NewsProvider(),
        ),
        ChangeNotifierProvider(
          create: (context) => NewsLatestProvider(),
        ),
        ChangeNotifierProvider(
          create: (context) => RapatPresensiProvider(),
        ),
      ],
      child: MultiBlocProvider(
        providers: [
          BlocProvider(
            create: (context) => PageCubit(),
          ),
        ],
        child: MaterialApp(
          debugShowCheckedModeBanner: false,
          // home: LoginPageFilkom(),
          routes: {
            '/': (context) => Splashscreen(),
            '/on-board-screen': (context) => Onboardscreen(),
            '/login-page-one': (context) => LoginPageOne(),
            '/login-page-filkom': (context) => LoginPage(),
            '/main-page': (context) => MainPage(),
            '/presensi-rapat-one': (context) => PresensiRapatOne(),
            '/presensi-rapat-two': (context) => PresensiRapatTwo(),
            // '/detil-rapat': (context) => RapatDetil(),
            // '/presensi-rapat-esign-one': (context) => PresensiRapatEsignOne(),
            '/presensi-rapat-esign-two': (context) => PresensiRapatEsignTwo(),
            '/profile-page': (context) => ProfilPage(),
            '/news-page': (context) => NewsPage(),
            '/signature-page': (context) => SignaturePage(),
          },
        ),
      ),
    );
  }
}
