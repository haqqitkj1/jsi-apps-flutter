import 'package:flutter/cupertino.dart';
import 'package:jsi_apps/models/news_model.dart';
import 'package:jsi_apps/services/data_service.dart';

class NewsProvider with ChangeNotifier {
  List<NewsModel> _newsModel = [];

  List<NewsModel> get news => _newsModel;

  set news(List<NewsModel> news) {
    _newsModel = news;
    notifyListeners();
  }

  Future<bool> getNews({
    required String accessToken,
  }) async {
    try {
      List<NewsModel> news = await DataService().getNews(
        accessToken: accessToken,
      );
      _newsModel = news;
      return true;
    } catch (e) {
      print(e);
      return false;
    }
  }
}
