import 'package:flutter/cupertino.dart';
import 'package:jsi_apps/models/user_model.dart';
import 'package:jsi_apps/services/auth_service.dart';

class UserProvider with ChangeNotifier {
  UserModel _userModel = UserModel();

  UserModel get userDet => _userModel;

  set userDet(UserModel user) {
    _userModel = user;
    notifyListeners();
  }

  Future<bool> getDetailUser({
    required String id,
    required String accessToken,
    // BuildContext? context,
  }) async {
    try {
      UserModel user = await AuthService().getDetailUser(
        id: id,
        accessToken: accessToken,
      );
      _userModel = user;
      return true;
    } catch (e) {
      print(e);
      return false;
    }
  }
}
