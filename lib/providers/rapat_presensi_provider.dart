import 'package:flutter/cupertino.dart';
import 'package:jsi_apps/models/auth_model.dart';
import 'package:jsi_apps/models/rapat_presensi_model.dart';
import 'package:jsi_apps/services/auth_service.dart';
import 'package:jsi_apps/services/data_service.dart';

class RapatPresensiProvider with ChangeNotifier {
  RapatPresensiModel _rapatPresensiModel = RapatPresensiModel();

  RapatPresensiModel get rapatPresensi => _rapatPresensiModel;

  set rapatPresensi(RapatPresensiModel rapatPresensiModel) {
    _rapatPresensiModel = rapatPresensiModel;
    notifyListeners();
  }

  Future<bool> postRapatPresensi({
    required String accessToken,
    required String username,
    required String tandaTangan,
    required String rapatId,
    required BuildContext context,
  }) async {
    try {
      // print(username);
      RapatPresensiModel rapatPresensiModel =
          await DataService().postRapatPresensi(
        accessToken: accessToken,
        username: username,
        tandaTangan: tandaTangan,
        rapatId: rapatId,
        context: context,
      );

      _rapatPresensiModel = rapatPresensiModel;
      return true;
    } catch (e) {
      print(e);
      return false;
    }
  }
}
