import 'package:flutter/cupertino.dart';
import 'package:jsi_apps/models/auth_model.dart';
import 'package:jsi_apps/services/auth_service.dart';

class AuthProvider with ChangeNotifier {
  AuthModel _authModel = AuthModel();

  AuthModel get user => _authModel;

  set user(AuthModel user) {
    _authModel = user;
    notifyListeners();
  }

  Future<bool> login({
    required String secret,
    required String username,
    required String password,
    // String? accessToken,
    // String? expiry,
    // String? id,
  }) async {
    try {
      // print(username);
      AuthModel user = await AuthService().authentication(
        secret: secret,
        username: username,
        password: password,
      );

      _authModel = user;
      return true;
    } catch (e) {
      print(e);
      return false;
    }
  }
}
