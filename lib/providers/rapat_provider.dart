import 'package:flutter/cupertino.dart';
import 'package:jsi_apps/models/rapat_model.dart';
import 'package:jsi_apps/services/data_service.dart';

class RapatProvider with ChangeNotifier {
  List<RapatModel> _rapatModel = [];

  List<RapatModel> get rapats => _rapatModel;

  set rapats(List<RapatModel> rapats) {
    _rapatModel = rapats;
    notifyListeners();
  }

  Future<bool> getRapat({
    required String accessToken,
    BuildContext? context,
    // BuildContext? context,
  }) async {
    try {
      List<RapatModel> rapats = await DataService().getRapat(
        accessToken: accessToken,
        context: context!,
      );
      _rapatModel = rapats;
      return true;
    } catch (e) {
      print(e);
      return false;
    }
  }
}
