import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:jsi_apps/models/auth_model.dart';
import 'package:jsi_apps/models/user_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AuthService {
  String baseUrl = 'https://apps-jsi.ub.ac.id/jsiapps/public/api';

  Future<AuthModel> authentication({
    required String secret,
    required String username,
    required String password,
  }) async {
    var url = '$baseUrl/get-token';
    var body = {
      'secret': secret,
      'username': username,
      'password': password,
    };

    var response = await http.post(Uri.parse(url), body: body);

    if (response.statusCode == 200) {
      // var jsonResp = json.decode(response.body)['api_status'];
      // if (jsonResp == 0) {
      //   Fluttertoast.showToast(
      //       msg: json.decode(response.body)['api_message'],
      //       toastLength: Toast.LENGTH_SHORT,
      //       gravity: ToastGravity.BOTTOM,
      //       timeInSecForIosWeb: 1,
      //       backgroundColor: Colors.red,
      //       textColor: Colors.white,
      //       fontSize: 16.0);
      // } else {
      Fluttertoast.showToast(
          msg: json.decode(response.body)['api_message'],
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.green,
          textColor: Colors.white,
          fontSize: 16.0);
      // }
      var data = jsonDecode(response.body)['data'];
      AuthModel authModel = AuthModel.fromJson(data);
      authModel.accessToken = data['access_token'];
      String accToken = data['access_token'].toString();
      String id = data['id'].toString();
      SharedPreferences accTok = await SharedPreferences.getInstance();
      accTok.setString('access_token_sh', accToken);
      accTok.setString('id_sp', id);
      return authModel;
    } else {
      Fluttertoast.showToast(
          msg: json.decode(response.body)['api_message'],
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
      throw Exception('Gagal Login');
    }
  }

  Future<UserModel> getDetailUser({
    required String id,
    required String accessToken,
    // BuildContext? context,
  }) async {
    var url = '$baseUrl/detail_user?id=' + id;
    var headers = {'Authorization': 'Bearer $accessToken'};

    var response = await http.get(Uri.parse(url), headers: headers);
    if (response.statusCode == 200) {
      var jsonResp = json.decode(response.body)['api_status'];
      if (jsonResp == 0) {
        Fluttertoast.showToast(
            msg: json.decode(response.body)['api_message'],
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);
      } else {
        Fluttertoast.showToast(
            msg: json.decode(response.body)['api_message'],
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.green,
            textColor: Colors.white,
            fontSize: 16.0);
      }
      var data = jsonDecode(response.body)['data'];
      UserModel userModel = UserModel.fromJson(data);
      userModel.nama = data['nama'];
      String id = data['id'];
      String nama = data['nama'];
      String prodi = data['prodi'];
      String email = data['email'];
      String status = data['status'];
      SharedPreferences userDataSP = await SharedPreferences.getInstance();
      userDataSP.setString('id_sp', id);
      userDataSP.setString('nama_sp', nama);
      userDataSP.setString('prodi_sp', prodi);
      userDataSP.setString('email_sp', email);
      userDataSP.setString('status_sp', status);
      return userModel;
    } else {
      Fluttertoast.showToast(
          msg: json.decode(response.body)['api_message'],
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
      // BuildContext? context;
      // Navigator.pushReplacementNamed(context!, '/login-page-one');
      throw Exception('Gagal Mendapat Data User');
    }
  }
}
