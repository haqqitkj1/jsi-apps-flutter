import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:jsi_apps/models/news_model.dart';
import 'package:jsi_apps/models/rapat_model.dart';
import 'package:jsi_apps/models/rapat_presensi_model.dart';
import 'package:jsi_apps/widgets/custom_alert_dialog_success.dart';
import 'package:logger/logger.dart';

class DataService {
  String baseUrl = 'https://apps-jsi.ub.ac.id/jsiapps/public/api';
  // BuildContext? context;

  Future<List<RapatModel>> getRapat({
    required String accessToken,
    BuildContext? context,
    // BuildContext? context,
  }) async {
    var url = '$baseUrl/rapat';
    var headers = {'Authorization': 'Bearer $accessToken'};

    var response = await http.get(Uri.parse(url), headers: headers);
    // print(response.body);
    if (response.statusCode == 200) {
      List data = jsonDecode(response.body)['data'];
      // print(data);
      List<RapatModel> rapat = [];
      for (var item in data) {
        rapat.add(RapatModel.fromJson(item));
      }
      // print(rapat);
      return rapat;
    } else if (response.statusCode == 403) {
      // Fluttertoast.showToast(
      //     msg: json.decode(response.body)['api_message'],
      //     toastLength: Toast.LENGTH_SHORT,
      //     gravity: ToastGravity.BOTTOM,
      //     timeInSecForIosWeb: 1,
      //     backgroundColor: Colors.red,
      //     textColor: Colors.white,
      //     fontSize: 16.0);
      var logger = Logger();
      logger.d(context);
      Navigator.pushReplacementNamed(context!, '/login-page-one');
      throw Exception('Token Expiry !');
    } else {
      Fluttertoast.showToast(
          msg: json.decode(response.body)['api_message'],
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
      throw Exception('Gagal Mendapat Data Rapat');
    }
  }

  Future<List<NewsModel>> getNews({
    required String accessToken,
  }) async {
    var url = '$baseUrl/beritajsi';
    var headers = {'Authorization': 'Bearer $accessToken'};

    var response = await http.get(Uri.parse(url), headers: headers);
    if (response.statusCode == 200) {
      List data = jsonDecode(response.body)['data'];
      // print(data);
      List<NewsModel> news = [];
      for (var item in data) {
        news.add(NewsModel.fromJson(item));
      }
      // print(news);
      return news;
    } else {
      Fluttertoast.showToast(
          msg: json.decode(response.body)['api_message'],
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
      // Get.to(LoginPageOne());
      throw Exception('Gagal Mendapat Data Berita');
    }
  }

  Future<List<NewsModel>> getLatestNews({
    required String accessToken,
  }) async {
    String limit = '5';
    var url = '$baseUrl/beritajsi?limit=' + limit + '&orderby=tanggal,desc';
    var headers = {'Authorization': 'Bearer $accessToken'};

    var response = await http.get(Uri.parse(url), headers: headers);
    if (response.statusCode == 200) {
      List data = jsonDecode(response.body)['data'];
      // print(data);
      List<NewsModel> news = [];
      for (var item in data) {
        news.add(NewsModel.fromJson(item));
      }
      return news;
    } else {
      Fluttertoast.showToast(
          msg: json.decode(response.body)['api_message'],
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
      // Get.to(LoginPageOne());
      throw Exception('Gagal Mendapat Data Berita');
    }
  }

  Future<RapatPresensiModel> postRapatPresensi({
    required String accessToken,
    required String username,
    required String tandaTangan,
    required String rapatId,
    required BuildContext context,
  }) async {
    var url = '$baseUrl/rapatpresensi';
    var headers = {'Authorization': 'Bearer $accessToken'};
    var body = {
      'nama': username,
      'tanda_tangan': tandaTangan,
      'notulen_rapat_id': rapatId,
    };

    var response = await http.post(
      Uri.parse(url),
      headers: headers,
      body: body,
    );
    if (response.statusCode == 200) {
      var data = jsonDecode(response.body)['data'];
      RapatPresensiModel rapatPresensiModel = RapatPresensiModel.fromJson(data);
      rapatPresensiModel.id = data['id'];
      Fluttertoast.showToast(
          msg:
              json.decode(response.body)['api_message'] + data['id'].toString(),
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);

      showDialog(
          context: context,
          builder: (BuildContext context) {
            return CustomAlertDialogSuccess(
              title: 'PRECENSION RECORDED SUCCESSFULLY',
              onPressed: () {
                int count = 0;
                Navigator.of(context).popUntil((_) => count++ >= 2);
              },
              icons: Icons.check_box,
              onPressedCancel: () {},
            );
          });
      return rapatPresensiModel;
    } else {
      Fluttertoast.showToast(
          msg: json.decode(response.body)['api_message'],
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
      throw Exception('Gagal Melakukan Presensi');
    }
  }
}
