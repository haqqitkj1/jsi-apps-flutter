import 'package:flutter/material.dart';
import 'package:jsi_apps/shared/custom_theme.dart';

class CustomButton extends StatelessWidget {
  final String title;
  final double width;
  final double fontSize;
  final Function() onPressed;
  final EdgeInsets margin;
  final Color bgColor;

  const CustomButton(
      {Key? key,
      required this.title,
      this.width = double.infinity,
      required this.onPressed,
      this.fontSize = 14,
      this.margin = EdgeInsets.zero,
      this.bgColor = Colors.red})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: 35,
      margin: margin,
      child: TextButton(
        onPressed: onPressed,
        style: TextButton.styleFrom(
            backgroundColor: bgColor,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(
              defaultRadius5,
            ))),
        child: Text(
          title,
          style: whiteTextStyle.copyWith(
            fontSize: fontSize,
            fontWeight: semibold,
          ),
        ),
      ),
    );
  }
}
