import 'package:flutter/material.dart';

class CustomLogoImage extends StatelessWidget {
  final double width;
  final double height;
  const CustomLogoImage({
    Key? key,
    this.width = 190,
    this.height = 131,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Image(
        image: AssetImage("assets/images/jsi_logo.png"),
        width: width,
        height: height,
      ),
    );
  }
}
