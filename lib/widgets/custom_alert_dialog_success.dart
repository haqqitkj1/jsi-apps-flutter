import 'package:flutter/material.dart';
import 'package:jsi_apps/shared/custom_theme.dart';
import 'package:jsi_apps/widgets/custom_logo_image.dart';

class CustomAlertDialogSuccess extends StatelessWidget {
  final String title;
  final IconData icons;
  final bool justOK;
  final Function() onPressed;
  final Function() onPressedCancel;
  const CustomAlertDialogSuccess({
    Key? key,
    required this.title,
    this.icons = Icons.android_outlined,
    this.justOK = true,
    required this.onPressed,
    required this.onPressedCancel,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      backgroundColor: pancho,
      child: Stack(
        overflow: Overflow.visible,
        alignment: Alignment.topCenter,
        // clipBehavior: Clip.hardEdge,
        children: [
          Container(
            height: 225,
            width: 250,
            child: Padding(
              padding: EdgeInsets.only(
                top: 60,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Icon(
                    icons,
                    size: 40,
                    color: fiord,
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                    title.toUpperCase(),
                    style: fiordTextStyle.copyWith(
                      fontSize: 15,
                      fontWeight: reguler,
                      letterSpacing: 2,
                    ),
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(
                    height: 38,
                  ),
                  Align(
                    alignment: Alignment.bottomRight,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        justOK
                            ? Container()
                            : GestureDetector(
                                onTap: onPressedCancel,
                                child: Text(
                                  'Cancel'.toUpperCase(),
                                  style: blackTextStyle.copyWith(
                                      fontSize: 15,
                                      fontWeight: black,
                                      shadows: [
                                        Shadow(
                                            color:
                                                Colors.black.withOpacity(0.3),
                                            offset: Offset(15, 15),
                                            blurRadius: 15),
                                      ]),
                                ),
                              ),
                        SizedBox(
                          width: defaultMargin,
                        ),
                        GestureDetector(
                          onTap: onPressed,
                          child: Text(
                            'OK'.toUpperCase(),
                            style: blackTextStyle.copyWith(
                                fontSize: 15,
                                fontWeight: black,
                                shadows: [
                                  Shadow(
                                      color: Colors.black.withOpacity(0.3),
                                      offset: Offset(15, 15),
                                      blurRadius: 15),
                                ]),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          Positioned(
            child: CustomLogoImage(
              height: 66,
              width: 95,
            ),
            top: -33,
          ),
        ],
      ),
    );
  }
}
