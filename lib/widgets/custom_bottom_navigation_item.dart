import 'package:flutter/material.dart';
import 'package:jsi_apps/cubit/page_cubit.dart';
import 'package:jsi_apps/shared/custom_theme.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CustomBottomNavigationItem extends StatelessWidget {
  final String imageUrl;
  final String title;
  final int index;
  const CustomBottomNavigationItem({
    Key? key,
    required this.index,
    required this.imageUrl,
    required this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        context.read<PageCubit>().setPage(index);
      },
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          SizedBox(),
          Container(
            width: context.read<PageCubit>().state == index ? 25 : 23,
            height: context.read<PageCubit>().state == index ? 25 : 23,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage(imageUrl),
              ),
            ),
          ),
          Text(
            title,
            style: fiordTextStyle.copyWith(
              fontSize: 13,
              fontWeight:
                  context.read<PageCubit>().state == index ? semibold : reguler,
            ),
          )
        ],
      ),
    );
  }
}
