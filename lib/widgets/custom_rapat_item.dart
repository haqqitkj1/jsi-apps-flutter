import 'package:flutter/material.dart';
import 'package:jsi_apps/models/rapat_model.dart';
import 'package:jsi_apps/pages/presensi_rapat_esign_one.dart';
import 'package:jsi_apps/pages/rapat_detil.dart';
import 'package:jsi_apps/shared/custom_theme.dart';

class CustomRapatItem extends StatelessWidget {
  final RapatModel rapat;
  // final String title;
  // final String date;
  // final String tempat;
  const CustomRapatItem({
    Key? key,
    required this.rapat,
    // this.title = 'Rapat JSI APPS',
    // this.date = '10 oktober 2021',
    // this.tempat = 'Daring',
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => RapatDetil(rapat: rapat),
          ),
        );
      },
      child: Container(
        margin: EdgeInsets.only(
          top: 15,
          left: defaultMargin,
          right: defaultMargin,
        ),
        width: double.infinity,
        height: 58,
        padding: EdgeInsets.symmetric(
          horizontal: 15,
          vertical: 10,
        ),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(
              defaultRadius10,
            ),
            border: Border.all(
              color: carrotOrange,
            ),
            color: kTransparency),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              rapat.judulRapat!.toUpperCase(),
              // title.toUpperCase(),
              style: fiordTextStyle.copyWith(
                fontWeight: semibold,
                fontSize: 14,
              ),
              overflow: TextOverflow.ellipsis,
            ),
            Row(
              children: [
                Text(
                  // rapat.judulRapat!.toUpperCase(),
                  rapat.tanggal! + '  ( ' + rapat.waktu! + ' )',
                  style: mineShaftTextStyle.copyWith(
                    fontWeight: semibold,
                    fontSize: 11,
                  ),
                  textAlign: TextAlign.center,
                  overflow: TextOverflow.ellipsis,
                ),
                SizedBox(
                  width: 5,
                ),
                Text(
                  // rapat.judulRapat!.toUpperCase(),
                  '-',
                  style: mineShaftTextStyle.copyWith(
                    fontWeight: semibold,
                    fontSize: 11,
                  ),
                  textAlign: TextAlign.center,
                  overflow: TextOverflow.ellipsis,
                ),
                SizedBox(
                  width: 5,
                ),
                Text(
                  // rapat.judulRapat!.toUpperCase(),
                  rapat.tempat!,
                  style: mineShaftTextStyle.copyWith(
                    fontWeight: semibold,
                    fontSize: 11,
                  ),
                  textAlign: TextAlign.center,
                  overflow: TextOverflow.ellipsis,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
