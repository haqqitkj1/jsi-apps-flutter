import 'package:flutter/material.dart';
import 'package:jsi_apps/shared/custom_theme.dart';

class CustomMenuPageItem extends StatelessWidget {
  final String imageUrl;
  final String title;
  final bool isActive;
  final Function() onPressed;

  const CustomMenuPageItem({
    Key? key,
    required this.isActive,
    this.imageUrl = 'assets/images/ic_comingsoon_menu.png',
    this.title = 'Comingsoon',
    required this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: isActive ? onPressed : () {},
      child: Column(
        children: [
          Image.asset(
            isActive ? imageUrl : 'assets/images/ic_comingsoon_menu.png',
            width: 150,
            height: 150,
          ),
          SizedBox(
            height: 10,
          ),
          Text(
            isActive ? title : 'Comingsoon',
            style: fiordTextStyle.copyWith(
              fontSize: 15,
              fontWeight: bold,
            ),
          )
        ],
      ),
    );
  }
}
