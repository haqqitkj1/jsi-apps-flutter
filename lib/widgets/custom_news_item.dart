import 'package:flutter/material.dart';
import 'package:jsi_apps/models/news_model.dart';
import 'package:jsi_apps/shared/custom_theme.dart';
import 'package:url_launcher/url_launcher.dart';

class CustomNewsItem extends StatelessWidget {
  final NewsModel news;
  // final String title;
  // final String date;
  const CustomNewsItem({Key? key, required this.news}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    void _launchURL() async => await canLaunch(news.link!)
        ? await launch(news.link!)
        : throw 'Could not launch ' + news.link!;
    return GestureDetector(
      onTap: () {
        _launchURL();
      },
      child: Container(
        margin: EdgeInsets.only(
          top: 15,
          left: defaultMargin,
          right: defaultMargin,
        ),
        width: double.infinity,
        height: 65,
        padding: EdgeInsets.all(
          10,
        ),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(
              defaultRadius10,
            ),
            border: Border.all(
              color: astronaut,
            ),
            color: kWhiteColor),
        child: Row(
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    news.judul!,
                    style: astronautTextStyle.copyWith(
                      fontWeight: semibold,
                      fontSize: 12,
                    ),
                    textAlign: TextAlign.start,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                  ),
                  Text(
                    news.tanggal!.toUpperCase(),
                    style: fiordTextStyle.copyWith(
                      fontWeight: reguler,
                      fontSize: 10,
                    ),
                  ),
                ],
              ),
            ),
            Icon(
              Icons.arrow_forward_ios_rounded,
              color: astronaut,
            )
          ],
        ),
      ),
    );
  }
}
