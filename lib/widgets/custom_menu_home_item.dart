import 'package:flutter/material.dart';
import 'package:jsi_apps/shared/custom_theme.dart';

class CustomMenuHomeItem extends StatelessWidget {
  final String imageUrl;
  final String title;
  final double height;
  final double width;
  const CustomMenuHomeItem({
    Key? key,
    required this.imageUrl,
    required this.title,
    this.height = 23,
    this.width = 23,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
          height: height,
          width: width,
          margin: EdgeInsets.only(bottom: 7),
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage(imageUrl),
            ),
          ),
        ),
        Text(
          title,
          style: fiordTextStyle.copyWith(
            fontSize: 13,
            fontWeight: semibold,
          ),
          textAlign: TextAlign.center,
        )
      ],
    );
  }
}
