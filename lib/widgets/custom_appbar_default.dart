import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:jsi_apps/shared/custom_theme.dart';

class CustomAppbarDefault extends StatelessWidget {
  final Color backgroundcolor;
  final bool contentDark;
  const CustomAppbarDefault({
    Key? key,
    this.backgroundcolor = Colors.white,
    this.contentDark = true,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Align(
        alignment: Alignment.topCenter,
        child: Container(
          padding: EdgeInsets.all(10),
          width: double.infinity,
          height: 59,
          decoration: BoxDecoration(
            color: backgroundcolor,
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              IconButton(
                icon: Icon(
                  Icons.menu,
                  color: contentDark ? Color(0xFF4B5C6B) : Color(0xFFFFFFFF),
                ),
                onPressed: () {
                  Fluttertoast.showToast(
                      msg: "Toast hi",
                      toastLength: Toast.LENGTH_SHORT,
                      gravity: ToastGravity.BOTTOM,
                      timeInSecForIosWeb: 1,
                      backgroundColor: Colors.red,
                      textColor: Colors.white,
                      fontSize: 16.0);
                },
              ),
              SizedBox(
                width: 20,
              ),
              Expanded(
                child: Text(
                  'JSI APPS',
                  style: contentDark
                      ? fiordTextStyle.copyWith(
                          fontSize: 20,
                          fontWeight: black,
                        )
                      : whiteTextStyle.copyWith(
                          fontSize: 20,
                          fontWeight: black,
                        ),
                ),
              ),
              IconButton(
                icon: Icon(
                  Icons.notifications,
                  color: contentDark ? Color(0xFF4B5C6B) : Color(0xFFFFFFFF),
                ),
                onPressed: () {
                  Fluttertoast.showToast(
                      msg: "Toast",
                      toastLength: Toast.LENGTH_SHORT,
                      gravity: ToastGravity.BOTTOM,
                      timeInSecForIosWeb: 1,
                      backgroundColor: Colors.red,
                      textColor: Colors.white,
                      fontSize: 16.0);
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
