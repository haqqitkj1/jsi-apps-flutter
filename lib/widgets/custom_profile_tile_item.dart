import 'package:flutter/material.dart';
import 'package:jsi_apps/shared/custom_theme.dart';

class CustomProfileTileItem extends StatelessWidget {
  final String title;
  final String value;
  const CustomProfileTileItem(
      {Key? key, required this.title, required this.value})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                title,
                style: blackTextStyle.copyWith(
                    fontSize: 11, fontWeight: reguler, letterSpacing: 3),
              ),
              SizedBox(
                height: 5,
              ),
              Text(
                value,
                style: blackTextStyle.copyWith(
                    fontSize: 12, fontWeight: semibold, letterSpacing: 3),
                textAlign: TextAlign.justify,
              ),
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.only(
            top: 5,
            bottom: 10,
          ),
          width: double.infinity,
          height: 2,
          decoration: BoxDecoration(
            color: Color(0xffC0C0C0),
            borderRadius: BorderRadius.circular(18),
          ),
        ),
      ],
    );
  }
}
