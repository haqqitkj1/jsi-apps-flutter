import 'package:flutter/material.dart';
import 'package:jsi_apps/shared/custom_theme.dart';

class CustomButtonLoading extends StatelessWidget {
  final String title;
  final double width;
  final double fontSize;
  final EdgeInsets margin;
  final Color bgColor;
  const CustomButtonLoading(
      {Key? key,
      required this.title,
      this.width = double.infinity,
      this.fontSize = 14,
      this.margin = EdgeInsets.zero,
      this.bgColor = Colors.green})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Wrap(
      children: [
        Container(
          width: width,
          height: 35,
          margin: margin,
          child: TextButton(
            onPressed: () {},
            style: TextButton.styleFrom(
                backgroundColor: bgColor,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(
                  defaultRadius5,
                ))),
            child: Row(
              children: [
                Container(
                    width: 10,
                    height: 10,
                    child: CircularProgressIndicator(
                      strokeWidth: 2,
                      valueColor: AlwaysStoppedAnimation(Colors.white),
                    )),
                SizedBox(
                  width: 3,
                ),
                Text(
                  'Loading',
                  style: whiteTextStyle.copyWith(
                    fontSize: fontSize,
                    fontWeight: reguler,
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
