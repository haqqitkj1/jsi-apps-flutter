import 'package:flutter/material.dart';
import 'package:jsi_apps/models/news_model.dart';
import 'package:jsi_apps/shared/custom_theme.dart';
import 'package:url_launcher/url_launcher.dart';

class NewsCard extends StatelessWidget {
  final NewsModel news;
  // final String title;
  final String imageurl;
  // final Function() onpressed;

  const NewsCard(
      {Key? key,
      this.imageurl = 'assets/images/image_news1.png',
      required this.news})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    void _launchURL() async => await canLaunch(news.link!)
        ? await launch(news.link!)
        : throw 'Could not launch ' + news.link!;

    Widget customShadow() {
      return Container(
        margin: EdgeInsets.only(top: 149),
        width: 375,
        height: 70,
        decoration: BoxDecoration(
            gradient: LinearGradient(
          colors: [kWhiteColor.withOpacity(0.50), astronaut.withOpacity(0.95)],
          begin: Alignment.centerRight,
          end: Alignment.centerLeft,
        )),
      );
    }

    Widget textTitle() {
      return Container(
        margin: EdgeInsets.only(top: 160, left: 15),
        width: 300,
        child: Text(
          news.judul!.toString(),
          style: whiteTextStyle.copyWith(
            fontSize: 13,
            fontWeight: bold,
          ),
          maxLines: 2,
          overflow: TextOverflow.ellipsis,
        ),
      );
    }

    return GestureDetector(
        onTap: _launchURL,
        child: Stack(
          children: [
            Image.network(
              news.gambar!.toString(),
              width: 375,
              height: 219,
              fit: BoxFit.fill,
            ),
            customShadow(),
            textTitle()
          ],
        )
        // Container(
        //   width: 375,
        //   height: 219,
        //   decoration: BoxDecoration(
        //     image: DecorationImage(
        //       fit: BoxFit.fill,
        //       image: Image.network(
        //         news.gambar!,
        //       ),
        //     ),
        //   ),
        // ),
        );
  }
}
