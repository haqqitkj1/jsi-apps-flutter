import 'package:flutter/material.dart';
import 'package:jsi_apps/shared/custom_theme.dart';

class CustomBackgroundPage extends StatelessWidget {
  const CustomBackgroundPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: double.infinity,
      decoration: BoxDecoration(
          gradient: LinearGradient(
        begin: Alignment.topRight,
        end: Alignment.bottomLeft,
        colors: <Color>[
          kWhiteColor,
          bottomBackground,
        ],
      )
          // image: DecorationImage(
          //   fit: BoxFit.cover,
          //   image: AssetImage('assets/images/bg_default.png'),
          // ),

          ),
    );
  }
}
