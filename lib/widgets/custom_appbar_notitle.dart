import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:jsi_apps/shared/custom_theme.dart';

class CustomAppbarNotitle extends StatelessWidget {
  final String title;
  final bool action;
  final bool isDark;
  const CustomAppbarNotitle({
    Key? key,
    this.title = '',
    this.action = true,
    this.isDark = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: isDark ? astronaut : kWhiteColor,
      actions: action
          ? [
              IconButton(
                icon: const Icon(
                  Icons.menu,
                  color: Color(0xFF4B5C6B),
                ),
                onPressed: () {
                  Fluttertoast.showToast(
                      msg: "Toast",
                      toastLength: Toast.LENGTH_SHORT,
                      gravity: ToastGravity.BOTTOM,
                      timeInSecForIosWeb: 1,
                      backgroundColor: Colors.red,
                      textColor: Colors.white,
                      fontSize: 16.0);
                },
              ),
            ]
          : [],
      title: Text(
        title,
        style: isDark
            ? whiteTextStyle.copyWith(
                fontSize: 20,
                fontWeight: black,
              )
            : fiordTextStyle.copyWith(
                fontSize: 20,
                fontWeight: black,
              ),
      ),
      leading: Transform.scale(
        scale: 0.6,
        child: IconButton(
          icon: Image.asset(
            'assets/images/ic_back.png',
            color: isDark ? kWhiteColor : fiord,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
    );
  }
}
