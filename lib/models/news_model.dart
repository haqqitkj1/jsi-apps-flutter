class NewsModel {
  String? id;
  String? tanggal;
  String? kategori;
  String? judul;
  String? gambar;
  String? berita;
  String? link;

  NewsModel({
    this.id,
    this.tanggal,
    this.kategori,
    this.judul,
    this.gambar,
    this.berita,
    this.link,
  });

  NewsModel.fromJson(Map<String, dynamic> json) {
    id = json['id'].toString();
    tanggal = json['tanggal'];
    kategori = json['kategori'];
    judul = json['judul'];
    gambar = json['gambar'];
    berita = json['berita'];
    link = json['link'];
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'tanggal': tanggal,
      'kategori': kategori,
      'judul': judul,
      'gambar': gambar,
      'berita': berita,
      'link': link,
    };
  }
}
