class RapatPresensiModel {
  int? id;

  RapatPresensiModel({
    this.id,
  });

  RapatPresensiModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
    };
  }
}
