class AuthModel {
  String? accessToken;
  String? expiry;
  String? id;

  AuthModel({
    this.accessToken = '',
    this.expiry = '',
    this.id = '',
  });

  AuthModel.fromJson(Map<String, dynamic> json) {
    accessToken = json['acceess_token'];
    expiry = json['expiry'].toString();
    id = json['id'].toString();
  }

  Map<String, dynamic> toJson() {
    return {
      'access_token': accessToken,
      'expiry': expiry,
      'id': id,
    };
  }
}
