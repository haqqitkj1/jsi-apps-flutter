import 'dart:convert';
import 'package:http/http.dart' as http;

class PostResult {
  String apiMessage;
  String accessToken;
  String id;

  PostResult({
    this.apiMessage = '',
    this.accessToken = '',
    this.id = '',
  });

  factory PostResult.createPostResult(Map<String, dynamic> object) {
    return PostResult(
      apiMessage: object['api_message'],
    );
  }

  static Future<PostResult> connectToAPI(
      String secret, String username, String password) async {
    String apiUrl = "https://apps-jsi.ub.ac.id/jsiapps/public/api/get-token";
    var apiResult = await http.post(Uri.parse(apiUrl),
        body: {"secret": secret, "username": username, "password": password});
    var jsonObject = json.decode(apiResult.body);
    return PostResult.createPostResult(jsonObject);
  }
}
