class UserModel {
  String? id;
  String? nama;
  String? prodi;
  String? email;
  String? status;

  UserModel({
    this.id,
    this.nama,
    this.prodi,
    this.email,
    this.status,
  });

  UserModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nama = json['nama'];
    prodi = json['prodi'];
    email = json['email'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'nama': nama,
      'prodi': prodi,
      'email': email,
      'status': status,
    };
  }
}
