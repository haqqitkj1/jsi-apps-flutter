import 'package:date_format/date_format.dart';

class RapatModel {
  String? id;
  String? unit;
  String? judulRapat;
  String? tanggal;
  String? waktu;
  String? tempat;
  String? agenda;
  String? notulen;
  String? status;
  String? filePendukung;
  String? fileTambahan;

  RapatModel({
    this.id,
    this.unit,
    this.judulRapat,
    this.tanggal,
    this.waktu,
    this.tempat,
    this.agenda,
    this.notulen,
    this.status,
    this.filePendukung,
    this.fileTambahan,
  });

  RapatModel.fromJson(Map<String, dynamic> json) {
    id = json['id'].toString();
    unit = json['unit'];
    judulRapat = json['judul_rapat'];
    DateTime dateTime = DateTime.parse(json['tanggal']);
    String formattedDate =
        formatDate(dateTime, [DD, ', ', dd, ' ', MM, ' ', yyyy]);
    tanggal = formattedDate;
    waktu = json['waktu'];
    tempat = json['tempat'];
    agenda = json['agenda'];
    notulen = json['notulen'];
    status = json['status'];
    filePendukung = json['file_pendukung'];
    fileTambahan = json['file_tambahan'];
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'unit': unit,
      'judul_rapat': judulRapat,
      'tanggal': tanggal,
      'waktu': waktu,
      'tempat': tempat,
      'agenda': agenda,
      'notulen': notulen,
      'status': status,
      'file_pendukung': filePendukung,
      'file_tambahan': fileTambahan,
    };
  }
}
