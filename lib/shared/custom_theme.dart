import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

// kode hex color
Color saphire = Color(0xFF2E4995);
Color astronaut = Color(0xFF223571);
Color supernova = Color(0xFFFFCC00);
Color carrotOrange = Color(0xffF29515);
Color fiord = Color(0xFF4B5C6B);
Color kBlueDark = Color(0xff1F1449);
Color kWhiteColor = Color(0xffffffff);
Color kBlackColor = Color(0xff000000);
Color kWhiteText = Color(0xffFAF6F6);
Color kTransparency = Colors.transparent;
Color pancho = Color(0xffEDD3AD);
Color mineShaft = Color(0xff302F2F);
Color bottomBackground = Color(0xffC4C4C4);

double defaultMargin = 20.0;
double defaultMarginAppbarContent = 55;
double defaultRadius5 = 5.0;
double defaultRadius10 = 10.0;

TextStyle saphireTextStyle = GoogleFonts.montserrat(
  color: saphire,
);

TextStyle fiordTextStyle = GoogleFonts.montserrat(
  color: fiord,
);

TextStyle supernovaTextStyle = GoogleFonts.montserrat(
  color: supernova,
);

TextStyle whiteTextStyle = GoogleFonts.montserrat(
  color: kWhiteText,
);

TextStyle blackTextStyle = GoogleFonts.montserrat(
  color: kBlackColor,
);

TextStyle mineShaftTextStyle = GoogleFonts.montserrat(
  color: mineShaft,
);

TextStyle astronautTextStyle = GoogleFonts.montserrat(
  color: astronaut,
);

FontWeight light = FontWeight.w300;
FontWeight reguler = FontWeight.w400;
FontWeight medium = FontWeight.w500;
FontWeight semibold = FontWeight.w600;
FontWeight bold = FontWeight.w700;
FontWeight extrabold = FontWeight.w800;
FontWeight black = FontWeight.w900;
