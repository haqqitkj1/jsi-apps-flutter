import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:jsi_apps/cubit/page_cubit.dart';
import 'package:jsi_apps/pages/menu_page.dart';
import 'package:jsi_apps/pages/presensi_rapat_esign_one.dart';
import 'package:jsi_apps/pages/presensi_rapat_esign_one.dart';
import 'package:jsi_apps/pages/presensi_rapat_one.dart';
import 'package:jsi_apps/pages/presensi_rapat_two.dart';
import 'package:jsi_apps/pages/profil_page.dart';
import 'package:jsi_apps/shared/custom_theme.dart';
import 'package:jsi_apps/widgets/custom_alert_dialog_success.dart';
import 'package:jsi_apps/widgets/custom_appbar_default.dart';
import 'package:jsi_apps/widgets/custom_background_pages.dart';
import 'package:jsi_apps/widgets/custom_bottom_navigation_item.dart';
import 'home_page.dart';

class MainPage extends StatelessWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget buildContent(int currentIndex) {
      switch (currentIndex) {
        case 0:
          return Container(
              padding: EdgeInsets.only(top: defaultMargin), child: HomePage());
        case 1:
          return MenuPage();
        case 2:
          return ProfilPage();
        default:
          return HomePage();
      }
    }

    Widget customBottomNav() {
      return Align(
        alignment: Alignment.bottomCenter,
        child: Container(
          padding: EdgeInsets.symmetric(
            horizontal: 25,
            vertical: 7,
          ),
          width: double.infinity,
          height: 59,
          decoration: BoxDecoration(
            color: carrotOrange,
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              CustomBottomNavigationItem(
                index: 0,
                imageUrl: 'assets/images/ic_nav_home.png',
                title: 'Beranda',
              ),
              CustomBottomNavigationItem(
                index: 1,
                imageUrl: 'assets/images/ic_nav_menu.png',
                title: 'Menu',
              ),
              CustomBottomNavigationItem(
                index: 2,
                imageUrl: 'assets/images/ic_nav_profile.png',
                title: 'Profile',
              ),
            ],
          ),
        ),
      );
    }

    Future<bool> showExitPopup() async {
      return await showDialog(
              context: context,
              builder: (BuildContext context) {
                return CustomAlertDialogSuccess(
                  title: 'Do you want to exit an App?',
                  onPressed: () {
                    Navigator.of(context).pop(true);
                  },
                  icons: Icons.logout,
                  onPressedCancel: () {
                    Navigator.of(context).pop(false);
                  },
                  justOK: false,
                );
              }) ??
          false; //if showDialouge had returned null, then return false
    }

    return BlocBuilder<PageCubit, int>(
      builder: (context, currentIndex) {
        return Scaffold(
          body: WillPopScope(
            onWillPop: showExitPopup,
            child: SafeArea(
              child: Stack(
                children: [
                  CustomBackgroundPage(),
                  ListView(
                    children: [
                      currentIndex != 2
                          ? SizedBox(
                              height: 50,
                            )
                          : Container(),
                      buildContent(currentIndex),
                    ],
                  ),
                  currentIndex != 2 ? CustomAppbarDefault() : Container(),
                  customBottomNav()
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
