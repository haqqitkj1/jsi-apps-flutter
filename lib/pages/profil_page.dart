import 'package:flutter/material.dart';
import 'package:jsi_apps/models/user_model.dart';
import 'package:jsi_apps/providers/user_provider.dart';
import 'package:jsi_apps/shared/custom_theme.dart';
import 'package:jsi_apps/widgets/custom_appbar_default.dart';
import 'package:jsi_apps/widgets/custom_profile_tile_item.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

String _id = '';
String _nama = '';
String _prodi = '';
String _email = '';
String _status = '';

class ProfilPage extends StatefulWidget {
  ProfilPage({Key? key}) : super(key: key);
  @override
  _ProfilPageState createState() => _ProfilPageState();
}

class _ProfilPageState extends State<ProfilPage> {
  getDataSP() async {
    SharedPreferences userDataSP = await SharedPreferences.getInstance();
    String id = userDataSP.getString('id_sp').toString();
    String nama = userDataSP.getString('nama_sp').toString();
    String prodi = userDataSP.getString('prodi_sp').toString();
    String email = userDataSP.getString('email_sp').toString();
    String status = userDataSP.getString('status_sp').toString();
    setState(() {
      _id = id;
      _nama = nama;
      _prodi = prodi;
      _email = email;
      _status = status;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getDataSP();
  }

  @override
  Widget build(BuildContext context) {
    UserProvider userProvider = Provider.of<UserProvider>(context);
    UserModel userModel = userProvider.userDet;
    // Widget customBottomNav() {
    //   return Align(
    //     alignment: Alignment.bottomCenter,
    //     child: Container(
    //       padding: EdgeInsets.symmetric(
    //         horizontal: 25,
    //         vertical: 7,
    //       ),
    //       width: double.infinity,
    //       height: 59,
    //       decoration: BoxDecoration(
    //         color: carrotOrange,
    //       ),
    //       child: Row(
    //         mainAxisAlignment: MainAxisAlignment.spaceAround,
    //         children: [
    //           CustomBottomNavigationItem(
    //             index: 0,
    //             imageUrl: 'assets/images/ic_nav_home.png',
    //             title: 'Beranda',
    //           ),
    //           CustomBottomNavigationItem(
    //             index: 1,
    //             imageUrl: 'assets/images/ic_nav_menu.png',
    //             title: 'Menu',
    //           ),
    //           CustomBottomNavigationItem(
    //             index: 2,
    //             imageUrl: 'assets/images/ic_nav_profile.png',
    //             title: 'Profile',
    //           ),
    //         ],
    //       ),
    //     ),
    //   );
    // }

    Widget buildContent() {
      return Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
          begin: Alignment.topRight,
          end: Alignment.bottomLeft,
          colors: <Color>[
            kWhiteColor,
            bottomBackground,
          ],
        )),
        child: Padding(
          padding: EdgeInsets.only(
            bottom: 50,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Stack(
                children: [
                  Column(
                    children: [
                      Container(
                        height: 300,
                        width: double.infinity,
                        decoration: BoxDecoration(
                          image: DecorationImage(
                              image:
                                  AssetImage('assets/images/bg_title_page.png'),
                              fit: BoxFit.cover),
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Container(
                              margin: EdgeInsets.all(defaultMargin),
                              child: Text(
                                "PROFIL SAYA",
                                style: whiteTextStyle.copyWith(
                                    fontSize: 20,
                                    fontWeight: bold,
                                    letterSpacing: 3),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        height: 87,
                        width: double.infinity,
                        margin: EdgeInsets.only(top: 3),
                        color: Color(0xff0A2A52),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Container(
                              margin: EdgeInsets.all(defaultMargin),
                              child: Column(
                                children: [
                                  Text(
                                    "Tenaga Pendidik",
                                    style: whiteTextStyle.copyWith(
                                        fontSize: 12,
                                        fontWeight: reguler,
                                        letterSpacing: 3),
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Text(
                                    // "${userModel.id}",
                                    _id != '' ? _id : '-',
                                    style: whiteTextStyle.copyWith(
                                        fontSize: 12,
                                        fontWeight: semibold,
                                        letterSpacing: 3),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Container(
                    width: 90,
                    height: 90,
                    padding: EdgeInsets.all(defaultMargin),
                    margin: EdgeInsets.only(top: 260, left: defaultMargin),
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: kWhiteColor,
                      image: DecorationImage(
                        image: AssetImage('assets/images/jsi_logo.png'),
                      ),
                    ),
                  ),
                  CustomAppbarDefault(
                    backgroundcolor: kTransparency,
                    contentDark: false,
                  ),
                ],
              ),
              Container(
                margin: EdgeInsets.symmetric(
                  vertical: 27,
                  horizontal: 10,
                ),
                padding: EdgeInsets.symmetric(
                  horizontal: 5,
                ),
                child: Column(
                  children: [
                    CustomProfileTileItem(
                      title: 'Nama',
                      value: _nama != '' ? _nama : '-',
                    ),
                    CustomProfileTileItem(
                      title: 'Email',
                      value: _email != '' ? _email : '-',
                    ),
                    CustomProfileTileItem(
                      title: 'Status',
                      value: _status != '' ? _status : '-',
                    ),
                    CustomProfileTileItem(
                        title: 'Jurusan', value: 'Jurusan Sistem Informasi'),
                    CustomProfileTileItem(
                      title: 'Program Studi',
                      value: _prodi != '' ? _prodi : 'User',
                    ),
                    CustomProfileTileItem(
                        title: 'KJFD',
                        value: 'KJFD Komputasi Berbasis Jaringan'),
                    CustomProfileTileItem(
                        title: 'Bidang Keilmuan',
                        value:
                            'Internet Of Things, Rural/Challenged Networks, Software Defined Networking, IPv6, IP Multicast, Content-Centric Networking Komputasi Berbasis Jaringan'),
                  ],
                ),
              ),
            ],
          ),
        ),
      );
    }

    return Container(
      child: Stack(children: [
        buildContent(),
      ]),
    );
  }
}
