import 'package:flutter/material.dart';
import 'package:jsi_apps/shared/custom_theme.dart';
import 'package:jsi_apps/widgets/custom_background_pages.dart';
import 'package:jsi_apps/widgets/custom_logo_image.dart';

class Onboardscreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              CustomLogoImage(),
              Padding(
                padding: EdgeInsets.only(top: 20.0),
                child: Text(
                  "Perkenalan Aplikasi",
                  style: saphireTextStyle.copyWith(
                    fontSize: 18,
                    fontWeight: black,
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 36.0),
                child: ElevatedButton(
                  onPressed: () {
                    Navigator.pushNamed(context, '/login-page-one');
                  },
                  child: Text(
                    'Mulai',
                    style: supernovaTextStyle.copyWith(
                      fontSize: 18,
                      fontWeight: black,
                    ),
                  ),
                  style: ElevatedButton.styleFrom(
                    primary: astronaut,
                    padding: EdgeInsets.only(
                        top: 5.0, bottom: 5.0, left: 45.0, right: 45.0),
                    // Set padding
                  ),
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}
