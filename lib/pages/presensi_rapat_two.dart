import 'package:flutter/material.dart';
import 'package:empty_widget/empty_widget.dart';
import 'package:jsi_apps/providers/rapat_provider.dart';
import 'package:jsi_apps/shared/custom_theme.dart';
import 'package:jsi_apps/widgets/custom_appbar_notitle.dart';
import 'package:jsi_apps/widgets/custom_background_pages.dart';
import 'package:provider/provider.dart';

import '../widgets/custom_rapat_item.dart';

class PresensiRapatTwo extends StatelessWidget {
  const PresensiRapatTwo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    RapatProvider rapatProvider = Provider.of<RapatProvider>(context);
    Widget rapatList() {
      return Column(
        children: rapatProvider.rapats
            .map((rapat) => CustomRapatItem(rapat: rapat))
            .toList(),
        // children: [
        //   CustomRapatItem(title: 'Rapat GBQ UB 2021'),
        //   CustomRapatItem(title: 'Rapat GBQ UB 2021'),
        //   CustomRapatItem(title: 'Rapat GBQ UB 2021'),
        // ],
      );
    }

    Widget buildContent() {
      return SingleChildScrollView(
        child: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topRight,
              end: Alignment.bottomLeft,
              colors: <Color>[
                kWhiteColor,
                bottomBackground,
              ],
            ),
          ),
          child: Padding(
              padding: EdgeInsets.only(
                bottom: defaultMargin,
              ),
              child: Column(
                children: [
                  Container(
                    height: 200,
                    width: double.infinity,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage('assets/images/bg_title_page.png'),
                          fit: BoxFit.cover),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Container(
                          margin: EdgeInsets.all(defaultMargin),
                          child: Text(
                            "Presensi Rapat",
                            style: whiteTextStyle.copyWith(
                                fontSize: 22,
                                fontWeight: bold,
                                letterSpacing: 3),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                      // left: 1,
                      // right: 1,
                      top: 10,
                    ),
                    child: Column(
                      children: [
                        rapatProvider.rapats.length != 0
                            ? rapatList()
                            : EmptyWidget(
                                // Image from project assets
                                image: "assets/images/default_image.png",

                                /// Image from package assets
                                /// Uncomment below line to use package assets
                                // packageImage: PackageImage.Image_1,
                                title: 'No Data Available',
                                subTitle: 'Contact your admin support',
                                titleTextStyle: astronautTextStyle.copyWith(
                                    fontSize: 22, fontWeight: bold),
                                subtitleTextStyle: saphireTextStyle.copyWith(
                                    fontSize: 14, fontWeight: reguler),
                                // Uncomment below statement to hide background animation
                                // hideBackgroundAnimation: true,)
                              ),
                      ],
                    ),
                  )
                ],
              )),
        ),
      );
    }

    return Scaffold(
      body: Stack(
        children: [
          CustomBackgroundPage(),
          SingleChildScrollView(
            child: Column(
              children: [
                CustomAppbarNotitle(), //! : Appbar masih bug,
                buildContent(),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
