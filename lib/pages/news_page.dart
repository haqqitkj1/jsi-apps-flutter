import 'package:flutter/material.dart';
import 'package:jsi_apps/providers/news_provider.dart';
import 'package:jsi_apps/widgets/custom_appbar_notitle.dart';
import 'package:jsi_apps/widgets/custom_background_pages.dart';
import 'package:jsi_apps/widgets/custom_news_item.dart';
import 'package:provider/provider.dart';

class NewsPage extends StatelessWidget {
  const NewsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    NewsProvider newsProvider = Provider.of<NewsProvider>(context);

    Widget buildContent() {
      return Padding(
          padding: EdgeInsets.only(
            bottom: 100,
          ),
          child: Column(
            children: newsProvider.news
                .map((news) => CustomNewsItem(news: news))
                .toList(),
          ));
    }

    return SafeArea(
      child: Scaffold(
        body: Stack(
          children: [
            CustomBackgroundPage(),
            SingleChildScrollView(
              child: Column(
                children: [
                  CustomAppbarNotitle(
                    title: 'Berita',
                    action: false,
                    isDark: true,
                  ), //! : Appbar masih bug,
                  buildContent(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
