import 'dart:convert';
import 'dart:developer';
import 'dart:typed_data';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:jsi_apps/models/auth_model.dart';
import 'package:jsi_apps/models/rapat_model.dart';
import 'package:jsi_apps/pages/signature_preview_page.dart';
import 'package:jsi_apps/providers/auth_provider.dart';
import 'package:jsi_apps/providers/rapat_presensi_provider.dart';
import 'package:jsi_apps/shared/custom_theme.dart';
import 'package:jsi_apps/widgets/custom_alert_dialog_success.dart';
import 'package:jsi_apps/widgets/custom_appbar_default.dart';
import 'package:jsi_apps/widgets/custom_appbar_notitle.dart';
import 'package:jsi_apps/widgets/custom_button.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:signature/signature.dart';
import 'package:logger/logger.dart';

String _accessToken = '';
String _id = '';

class PresensiRapatEsignOne extends StatefulWidget {
  // final RapatModel rapat;
  final String judulRapat;
  final String id;
  PresensiRapatEsignOne(this.judulRapat, this.id);
  @override
  _PresensiRapatEsignOne createState() => _PresensiRapatEsignOne();
}

bool _isLoading = false;

class _PresensiRapatEsignOne extends State<PresensiRapatEsignOne> {
  getDataSP() async {
    SharedPreferences accTok = await SharedPreferences.getInstance();
    String id = accTok.getString('id').toString();
    String accessToken = accTok.getString('access_token_sh').toString();
    setState(() {
      _accessToken = accessToken;
      _id = id;
    });
  }

  File? _selectedFile;
  bool _inProcess = false;

  final SignatureController controller = SignatureController(
    penStrokeWidth: 3,
    penColor: Colors.black,
    exportBackgroundColor: kWhiteColor,
  );

  @override
  void initState() {
    super.initState();
    getDataSP();
    controller.addListener(() => print('Value changed'));
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  Future<Uint8List?> exportSignature() async {
    final exportController = SignatureController(
      penStrokeWidth: 2,
      penColor: Colors.black,
      exportBackgroundColor: Colors.white,
      points: controller.points,
    );

    final signature = await exportController.toPngBytes();
    exportController.dispose();

    return signature;
  }

  Future<Uint8List?> exportSignatureAsBase64() async {
    final exportController = SignatureController(
      penStrokeWidth: 2,
      penColor: Colors.black,
      exportBackgroundColor: Colors.white,
      points: controller.points,
    );

    final signature = await exportController.toPngBytes();
    // String bs64 = base64Encode(signature);
    exportController.dispose();

    return signature;
  }

  getImage() async {
    this.setState(() {
      _inProcess = true;
    });
    XFile? image = await ImagePicker().pickImage(source: ImageSource.gallery);
    if (image != null) {
      File? cropped = await ImageCropper.cropImage(
          sourcePath: image.path,
          aspectRatio: CropAspectRatio(ratioX: 1, ratioY: 1),
          compressQuality: 100,
          maxWidth: 700,
          maxHeight: 700,
          compressFormat: ImageCompressFormat.jpg,
          androidUiSettings: AndroidUiSettings(
            toolbarColor: Colors.deepOrange,
            toolbarTitle: "RPS Cropper",
            statusBarColor: Colors.deepOrange.shade900,
            backgroundColor: Colors.white,
          ));

      this.setState(() {
        _selectedFile = cropped!;
        _inProcess = false;
      });
    } else {
      this.setState(() {
        _inProcess = false;
      });
    }
  }

  Widget getImageWidget() {
    if (_selectedFile != null) {
      return Image.file(
        _selectedFile!,
        width: 250,
        height: 250,
        fit: BoxFit.cover,
      );
    } else {
      return Signature(
        controller: controller,
        height: 290,
        width: MediaQuery.of(context).size.width - 60,
        backgroundColor: Colors.white,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    AuthProvider authProvider = Provider.of<AuthProvider>(context);
    AuthModel authModel = authProvider.user;
    // var logger = Logger();
    // logger.d(authModel.accessToken, authModel.id);
    RapatPresensiProvider rapatPresensiProvider =
        Provider.of<RapatPresensiProvider>(context);

    postPresensi(String accesToken, username, tandaTangan, notulenRapatid,
        BuildContext context) async {
      setState(() {
        _isLoading = true;
      });

      if (await rapatPresensiProvider.postRapatPresensi(
          accessToken: accesToken,
          username: username,
          tandaTangan: tandaTangan,
          rapatId: notulenRapatid,
          context: context)) {
      } else {}

      setState(() {
        _isLoading = false;
      });
    }

    return Scaffold(
      body: SafeArea(
        child: ListView(children: [
          // CustomAppbarDefault(),
          CustomAppbarNotitle(
            title: 'E-Sign',
          ),
          Padding(
            padding: EdgeInsets.only(
              left: defaultMargin,
              right: defaultMargin,
              top: 16,
              bottom: 100,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "E-Sign",
                      style: fiordTextStyle.copyWith(
                        fontSize: 18,
                        fontWeight: semibold,
                      ),
                    ),
                    SizedBox(
                      height: 11,
                    ),
                    Text(
                      widget.id + ' ---- ' + widget.judulRapat,
                      style: fiordTextStyle.copyWith(
                        fontSize: 15,
                        fontWeight: semibold,
                      ),
                    ),
                    SizedBox(
                      height: 11,
                    ),
                    Center(
                      //TODO: Masih bentuk Image
                      child: Container(
                        height: 300,
                        width: double.infinity,
                        padding: EdgeInsets.all(4),
                        child: Stack(
                          children: [
                            Center(
                              child: getImageWidget(),
                            ),
                            Align(
                              alignment: Alignment.topRight,
                              child: GestureDetector(
                                onTap: () {
                                  if (controller.isNotEmpty) {
                                    controller.clear();
                                  } else if (_selectedFile != null) {
                                    _selectedFile =
                                        null; //! Selected file menggunakan set state / reload
                                    setState(() {});
                                  }
                                  // initState();
                                },
                                child: Container(
                                  width: 21,
                                  height: 35,
                                  margin: EdgeInsets.only(
                                    right: defaultMargin,
                                    top: 10,
                                  ),
                                  decoration: BoxDecoration(
                                    image: DecorationImage(
                                      image: AssetImage(
                                          'assets/images/ic_rapat_pen.png'),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        decoration: BoxDecoration(
                            border: Border.all(
                              color: fiord,
                              width: 1.5,
                            ),
                            borderRadius: BorderRadius.circular(10)),
                      ),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        GestureDetector(
                          onTap: getImage,
                          child: Icon(
                            Icons.image,
                            color: fiord,
                            size: 35,
                          ),
                        ),
                        CustomButton(
                          title: 'Submit',
                          onPressed: () async {
                            if (controller.isNotEmpty ||
                                _selectedFile != null) {
                              final signature = await exportSignature();
                              String signature2 = "data:image/png;base64," +
                                  base64.encode(signature!);
                              // controller.clear();
                              // postPresensi(authModel.accessToken.toString(),
                              //     authModel.id, tandaTangan, notulenRapatid)
                              // Fluttertoast.showToast(
                              //     msg: authModel.accessToken.toString() +
                              //         ' --- ' +
                              //         authModel.id! +
                              //         ' --- ' +
                              //         signature2.toString(),
                              //     toastLength: Toast.LENGTH_SHORT,
                              //     gravity: ToastGravity.BOTTOM,
                              //     timeInSecForIosWeb: 1,
                              //     backgroundColor: Colors.red,
                              //     textColor: Colors.white,
                              //     fontSize: 16.0);
                              // print(signature2.toString());
                              // // log(signature2.toString());
                              // var logger = Logger();

                              // logger.d(widget.id.toString());

                              //* Main Function of Post presensi
                              postPresensi(
                                  // authModel.accessToken.toString(),
                                  // authModel.id!.toString(),
                                  _accessToken,
                                  _id,
                                  signature2.toString(),
                                  widget.id.toString(),
                                  context);
                            } else {
                              Fluttertoast.showToast(
                                  msg: "Please Fill in the Fields",
                                  toastLength: Toast.LENGTH_SHORT,
                                  gravity: ToastGravity.BOTTOM,
                                  timeInSecForIosWeb: 1,
                                  backgroundColor: Colors.red,
                                  textColor: Colors.white,
                                  fontSize: 16.0);
                            }
                          },
                          width: 86,
                          bgColor: carrotOrange,
                        ),
                        CustomButton(
                          title: 'Cancel',
                          onPressed: () {
                            Navigator.pop(context);
                            // Fluttertoast.showToast(
                            //     msg: "Cancel",
                            //     toastLength: Toast.LENGTH_SHORT,
                            //     gravity: ToastGravity.BOTTOM,
                            //     timeInSecForIosWeb: 1,
                            //     backgroundColor: Colors.red,
                            //     textColor: Colors.white,
                            //     fontSize: 16.0);
                          },
                          width: 86,
                          bgColor: fiord,
                        ),
                        GestureDetector(
                          onTap: () async {
                            if (controller.isNotEmpty) {
                              final signature = await exportSignature();

                              await Navigator.of(context)
                                  .push(MaterialPageRoute(
                                builder: (context) {
                                  Uint8List signature2 = signature!;
                                  return SignaturePreviewPage(
                                      signature: signature2);
                                },
                              ));

                              controller.clear();
                            }
                          },
                          child: Icon(
                            Icons.download,
                            color: fiord,
                            size: 35,
                          ),
                        ),
                      ],
                    )
                  ],
                ),
                SizedBox(
                  height: 16,
                ),
                // rapatList(),
              ],
            ),
          ),
        ]),
      ),
    );
  }
}
