import 'package:flutter/material.dart';
import 'package:jsi_apps/widgets/custom_rapat_item.dart';
import 'package:jsi_apps/shared/custom_theme.dart';

//! DEPRECATED
class PresensiRapatOne extends StatelessWidget {
  const PresensiRapatOne({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget rapatList() {
      return Column(
        children: [
          // CustomRapatItem(title: 'Rapat GBQ UB 2021'),
          // CustomRapatItem(title: 'Rapat Akreditasi filkom JSI'),
          // CustomRapatItem(title: 'Rapat JSI Mobile'),
          // CustomRapatItem(title: 'Rapat TAHUNAN MPRRI'),
          // CustomRapatItem(title: 'Rapat Akreditasi filkom TIF'),
          // CustomRapatItem(title: 'Rapat GBQ UB 2021'),
          // CustomRapatItem(title: 'Rapat Akreditasi filkom JSI'),
          // CustomRapatItem(title: 'Rapat JSI Mobile'),
          // CustomRapatItem(title: 'Rapat TAHUNAN MPRRI'),
          // CustomRapatItem(title: 'Rapat Akreditasi filkom TIF'),
          // CustomRapatItem(title: 'Rapat GBQ UB 2021'),
          // CustomRapatItem(title: 'Rapat Akreditasi filkom JSI'),
          // CustomRapatItem(title: 'Rapat JSI Mobile'),
          // CustomRapatItem(title: 'Rapat TAHUNAN MPRRI'),
          // CustomRapatItem(title: 'Rapat Akreditasi filkom TIF'),
        ],
      );
    }

    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.only(
              left: defaultMargin,
              right: defaultMargin,
              top: 16,
              bottom: 100,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Presensi Rapat",
                      style: fiordTextStyle.copyWith(
                        fontSize: 15,
                        fontWeight: semibold,
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Container(
                        height: 24,
                        width: 24,
                        decoration: BoxDecoration(
                          image: DecorationImage(
                            image: AssetImage('assets/images/ic_back.png'),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 16,
                ),
                rapatList(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
