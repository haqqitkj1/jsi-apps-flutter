import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:jsi_apps/models/auth_model.dart';
import 'package:jsi_apps/models/user_model.dart';
import 'package:jsi_apps/providers/auth_provider.dart';
import 'package:jsi_apps/providers/user_provider.dart';
import 'package:jsi_apps/shared/custom_theme.dart';
import 'package:jsi_apps/widgets/custom_button.dart';
import 'package:jsi_apps/widgets/custom_button_loading.dart';
import 'package:provider/provider.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

TextEditingController usernameController = new TextEditingController();
TextEditingController passwordController = new TextEditingController();

bool _isLoading = false;

class _LoginPageState extends State<LoginPage> {
  //==========Widget==========//
  @override
  Widget build(BuildContext context) {
    AuthProvider authProvider = Provider.of<AuthProvider>(context);
    //==========Func==========//
    login(String secret, username, password) async {
      setState(() {
        _isLoading = true;
      });

      if (await authProvider.login(
        secret: secret,
        username: username,
        password: password,
      )) {
        // Navigator.pushReplacementNamed(context, '/main-page');
        AuthModel user = authProvider.user;
        UserProvider userProvider =
            Provider.of<UserProvider>(context, listen: false);
        // UserModel userModel = userProvider.userDet;
        // print('${user.accessToken}'.toString());
        // print('${user.id}'.toString());
        //==========Func==========//
        if (await userProvider.getDetailUser(
          id: '${user.id}'.toString(),
          accessToken: '${user.accessToken}'.toString(),
        )) {
          // UserProvider userProvider =
          //     Provider.of<UserProvider>(context, listen: false);
          // UserModel userModel = userProvider.userDet;
          Navigator.pushReplacementNamed(context, '/main-page');
        }

        // getDetailUser(String id, String accessToken) async {
        //   if (await userProvider.getDetailUser(
        //     id: id,
        //     accessToken: accessToken,
        //   )) {
        //     Fluttertoast.showToast(
        //         msg: 'Success get Data User',
        //         toastLength: Toast.LENGTH_SHORT,
        //         gravity: ToastGravity.BOTTOM,
        //         timeInSecForIosWeb: 1,
        //         backgroundColor: Colors.red,
        //         textColor: Colors.white,
        //         fontSize: 16.0);
        //   }
        // }
        //==========EndFunc==========//
      } else {
        // Fluttertoast.showToast(
        //     msg: 'Login Failed',
        //     toastLength: Toast.LENGTH_SHORT,
        //     gravity: ToastGravity.BOTTOM,
        //     timeInSecForIosWeb: 1,
        //     backgroundColor: Colors.red,
        //     textColor: Colors.white,
        //     fontSize: 16.0);
      }

      setState(() {
        _isLoading = false;
      });

      // String apiUrl = "https://apps-jsi.ub.ac.id/jsiapps/public/api/get-token";
      // Map data = {
      //   'secret': secret,
      //   'username': username,
      //   'password': password,
      // };
      // var jsonData;
      // var response = await http.post(Uri.parse(apiUrl), body: data);

      // if (response.statusCode == 200) {
      //   var jsonResp = json.decode(response.body)['api_status'];
      //   jsonResp == 0
      //       ? setState(() {
      //           _isLoading = false;
      //           Fluttertoast.showToast(
      //               msg: json.decode(response.body)['api_message'],
      //               toastLength: Toast.LENGTH_SHORT,
      //               gravity: ToastGravity.BOTTOM,
      //               timeInSecForIosWeb: 1,
      //               backgroundColor: Colors.red,
      //               textColor: Colors.white,
      //               fontSize: 16.0);
      //         })
      //       : setState(() async {
      //           jsonData = json.decode(response.body)['data'];
      //           _isLoading = false;
      //           print(jsonData["access_token"]);
      //           SharedPreferences accTok =
      //               await SharedPreferences.getInstance();
      //           accTok.setString('access_token_sh', jsonData);
      //           Fluttertoast.showToast(
      //               msg: json.decode(response.body)['api_message'],
      //               toastLength: Toast.LENGTH_SHORT,
      //               gravity: ToastGravity.BOTTOM,
      //               timeInSecForIosWeb: 1,
      //               backgroundColor: Colors.green,
      //               textColor: Colors.white,
      //               fontSize: 16.0);
      //           Navigator.pushReplacementNamed(context, '/main-page');
      //         });
      // } else {
      //   print(response.body);
      //   setState(() {
      //     _isLoading = false;
      //     Fluttertoast.showToast(
      //         msg: json.decode(response.body)['api_message'],
      //         toastLength: Toast.LENGTH_SHORT,
      //         gravity: ToastGravity.BOTTOM,
      //         timeInSecForIosWeb: 1,
      //         backgroundColor: Colors.red,
      //         textColor: Colors.white,
      //         fontSize: 16.0);
      //   });
      // }
    }

    //==========EndFunc==========//
    Widget backgroundImage() {
      return Container(
        width: double.infinity,
        height: double.infinity,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topRight,
            end: Alignment.bottomLeft,
            colors: <Color>[
              pancho,
              Color(0xFFA9B8D4),
            ],
          ),
        ),
      );
    }

    Widget loginInputSection(
      String title,
      String hint,
      bool obsecure,
      TextEditingController controller,
    ) {
      return Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              title,
              style: fiordTextStyle.copyWith(
                fontSize: 14,
                fontWeight: semibold,
              ),
            ),
            SizedBox(
              height: 6,
            ),
            TextFormField(
              controller: controller,
              cursorColor: astronaut,
              obscureText: obsecure,
              style: fiordTextStyle.copyWith(color: kBlueDark),
              decoration: InputDecoration(
                filled: true,
                fillColor: kWhiteColor,
                contentPadding: EdgeInsets.fromLTRB(10, 10, 10, 0),
                hintText: hint,
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(defaultRadius5),
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(
                    defaultRadius5,
                  ),
                  borderSide: BorderSide(color: astronaut, width: 2),
                ),
              ),
            )
          ],
        ),
      );
    }
    //==========EndWidget==========//

    return Scaffold(
      body: Stack(
        children: [
          backgroundImage(),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 35),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Sign In',
                  style: fiordTextStyle.copyWith(
                    fontSize: 16,
                    fontWeight: bold,
                  ),
                ),
                SizedBox(
                  height: 34,
                ),
                loginInputSection(
                    'Username', 'Your Username', false, usernameController),
                SizedBox(
                  height: 21,
                ),
                loginInputSection(
                    'Password', 'Your Password', true, passwordController),
                SizedBox(
                  height: 44,
                ),
                Center(
                  child: _isLoading
                      ? CustomButtonLoading(
                          title: 'Loading',
                          width: 86,
                        )
                      : CustomButton(
                          title: 'Sign In',
                          onPressed: () {
                            login(
                                "6ad9e973117f59d8732be82a8238f022",
                                usernameController.text,
                                passwordController.text);
                          },
                          width: 86,
                          bgColor: carrotOrange,
                        ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
