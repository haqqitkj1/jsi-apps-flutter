import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:jsi_apps/models/auth_model.dart';
import 'package:jsi_apps/models/news_model.dart';
import 'package:jsi_apps/models/user_model.dart';
import 'package:jsi_apps/pages/news_page.dart';
import 'package:jsi_apps/providers/auth_provider.dart';
import 'package:jsi_apps/providers/news_latest_provider.dart';
import 'package:jsi_apps/providers/news_provider.dart';
import 'package:jsi_apps/providers/rapat_provider.dart';
import 'package:jsi_apps/providers/user_provider.dart';
import 'package:jsi_apps/widgets/custom_button.dart';
import 'package:jsi_apps/widgets/custom_news_card.dart';
import 'package:jsi_apps/shared/custom_theme.dart';
import 'package:jsi_apps/widgets/custom_menu_home_item.dart';
import 'package:jsi_apps/widgets/custom_news_item.dart';
import 'package:logger/logger.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

String _nama = '';

class HomePage extends StatefulWidget {
  HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  getInitDataUser() async {
    AuthProvider authProvider = Provider.of<AuthProvider>(context);
    AuthModel authModel = authProvider.user;
    SharedPreferences accTok = await SharedPreferences.getInstance();
    String accessToken = accTok.getString('access_token_sh').toString();
    // print(accessToken);
    var logger = Logger();
    logger.d(authModel.id, authModel.accessToken);
    await Provider.of<UserProvider>(context, listen: false).getDetailUser(
      accessToken: accessToken,
      id: authModel.id!.toString(),
    );
  }

  getInitNews() async {
    SharedPreferences accTok = await SharedPreferences.getInstance();
    String accessToken = accTok.getString('access_token_sh').toString();
    // print(accessToken);
    await Provider.of<NewsProvider>(context, listen: false)
        .getNews(accessToken: accessToken);
  }

  getInitRapat() async {
    SharedPreferences accTok = await SharedPreferences.getInstance();
    String accessToken = accTok.getString('access_token_sh').toString();
    // print(accTok.getString('access_token_sh').toString());
    await Provider.of<RapatProvider>(context, listen: false)
        .getRapat(accessToken: accessToken, context: context);

    // Navigator.pushReplacementNamed(context, '/main-page');
  }

  getInitLatestNews() async {
    SharedPreferences accTok = await SharedPreferences.getInstance();
    String accessToken = accTok.getString('access_token_sh').toString();
    // print(accessToken);
    await Provider.of<NewsLatestProvider>(context, listen: false)
        .getLatestNews(accessToken: accessToken);
  }

  getNama() async {
    SharedPreferences userDataSP = await SharedPreferences.getInstance();
    String nama = userDataSP.getString('nama_sp').toString();
    setState(() {
      _nama = nama;
    });
  }

  @override
  void initState() {
    getInitRapat();
    getInitNews();
    getInitLatestNews();
    super.initState();
    // getInitDataUser();
    getNama();
  }

  @override
  Widget build(BuildContext context) {
    UserProvider userProvider = Provider.of<UserProvider>(context);
    UserModel userModel = userProvider.userDet;
    // AuthProvider authProvider = Provider.of<AuthProvider>(context);
    // AuthModel authModel = authProvider.user;

    NewsLatestProvider newsLatestProvider =
        Provider.of<NewsLatestProvider>(context);
    NewsProvider newsProvider = Provider.of<NewsProvider>(context);

    // var logger = Logger();
    // logger.d(newsProvider.news.length);
    // if (newsProvider.news.length != 0) {
    //   print('user_active');
    //   // getInitRapat();
    //   // getInitNews();
    //   // getInitLatestNews();
    // } else {
    //   // Navigator.pushReplacementNamed(context, '/login-page-one');
    //   SchedulerBinding.instance!.addPostFrameCallback((_) {
    //     Navigator.pushReplacementNamed(context, '/login-page-one');
    //   });
    // }

    // print('${userModel.nama}');

    //==========Func==========//
    // getDetailUser(String id, String accessToken) async {
    //   if (await userProvider.getDetailUser(
    //     id: id,
    //     accessToken: accessToken,
    //   )) {
    //     Fluttertoast.showToast(
    //         msg: 'Success get Data User',
    //         toastLength: Toast.LENGTH_SHORT,
    //         gravity: ToastGravity.BOTTOM,
    //         timeInSecForIosWeb: 1,
    //         backgroundColor: Colors.red,
    //         textColor: Colors.white,
    //         fontSize: 16.0);
    //   }
    // }
    //==========EndFunc==========//

    Widget header() {
      return Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.only(left: defaultMargin),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Assalamualaikum,',
                    style: fiordTextStyle.copyWith(
                      fontSize: 15,
                      fontWeight: reguler,
                    ),
                  ),
                  Text(
                    // userModel.nama != null ? '${userModel.nama}' : 'User',
                    _nama != '' ? _nama : 'User',
                    style: fiordTextStyle.copyWith(
                      fontSize: 15,
                      fontWeight: semibold,
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      );
    }

    Widget headlines() {
      return Container(
        margin: EdgeInsets.only(
          top: 16,
          bottom: 30,
        ),
        child: Stack(
          children: [
            SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  children: newsProvider.news
                      .map((news) => NewsCard(news: news))
                      .toList(),
                )),
            Container(
              margin: EdgeInsets.only(
                top: 200,
                right: defaultMargin,
              ),
              alignment: Alignment.bottomRight,
              child: CustomButton(
                title: 'Semua Berita',
                width: 107,
                fontSize: 12,
                bgColor: astronaut,
                onPressed: () {
                  Navigator.pushNamed(context, '/news-page');
                },
              ),
            ),
          ],
        ),
      );
    }

    Widget latestNews() {
      return Container(
        margin: EdgeInsets.only(
          top: 23,
          bottom: 30,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              margin: EdgeInsets.symmetric(horizontal: defaultMargin),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Berita terkini',
                    style: fiordTextStyle.copyWith(
                      fontSize: 15,
                      fontWeight: semibold,
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      Navigator.pushNamed(context, '/news-page');
                    },
                    child: Text(
                      'semua berita',
                      style: fiordTextStyle.copyWith(
                        decoration: TextDecoration.underline,
                        fontSize: 15,
                        fontWeight: reguler,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Column(
              children: newsLatestProvider.news
                  .map((news) => CustomNewsItem(news: news))
                  .toList(),
            )
          ],
        ),
      );
    }

    Widget menuHome() {
      return Container(
        margin: EdgeInsets.symmetric(
          horizontal: defaultMargin,
        ),
        width: double.infinity,
        height: 189,
        padding: EdgeInsets.symmetric(
          horizontal: 8,
          vertical: 23,
        ),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(
            defaultRadius10,
          ),
          color: Color(0xffF8F8F8),
        ),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                CustomMenuHomeItem(
                  imageUrl: 'assets/images/ic_menu_eye.png',
                  title: 'Visi Misi',
                ),
                CustomMenuHomeItem(
                  imageUrl: 'assets/images/ic_menu_bank.png',
                  title: 'Jurusan\nDan Prodi',
                ),
                CustomMenuHomeItem(
                  imageUrl: 'assets/images/ic_menu_pimpinan.png',
                  title: 'Pimpinan',
                  height: 27,
                ),
                CustomMenuHomeItem(
                  imageUrl: 'assets/images/ic_menu_computer.png',
                  title: 'Fasilitas',
                ),
              ],
            ),
            SizedBox(
              height: 17,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                CustomMenuHomeItem(
                  imageUrl: 'assets/images/ic_menu_calender.png',
                  title: 'Kalender\nAkademik',
                ),
                CustomMenuHomeItem(
                  imageUrl: 'assets/images/ic_menu_statistic.png',
                  title: 'Statistik',
                ),
                CustomMenuHomeItem(
                  imageUrl: 'assets/images/ic_menu_time.png',
                  title: 'Sejarah',
                ),
                CustomMenuHomeItem(
                  imageUrl: 'assets/images/ic_menu_jsi.png',
                  title: 'Lambang',
                  width: 34,
                ),
              ],
            ),
          ],
        ),
      );
    }

    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          header(),
          headlines(),
          menuHome(),
          latestNews(),
          SizedBox(
            height: 50,
          )
        ],
      ),
    );
  }
}
