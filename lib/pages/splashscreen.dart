import 'dart:async';
import 'dart:developer';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:jsi_apps/providers/rapat_provider.dart';
import 'package:jsi_apps/shared/custom_theme.dart';
import 'package:jsi_apps/widgets/custom_logo_image.dart';
import 'package:logger/logger.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Splashscreen extends StatefulWidget {
  @override
  _SplashscreenState createState() => _SplashscreenState();
}

class _SplashscreenState extends State<Splashscreen> {
  checkStateInstall() async {
    var _duration = new Duration(seconds: 3);

    SharedPreferences prefs = await SharedPreferences.getInstance();

    SharedPreferences accTok = await SharedPreferences.getInstance();
    // counter==0?
    bool? firstTime = prefs.getBool('first_time');
    if (firstTime != null && !firstTime) {
      // Not first time
      print('not firstTime');
      var logger = Logger();
      logger.d(accTok.getString('access_token_sh'));
      // Timer(_duration, () {
      // Navigator.pushReplacementNamed(context, '/main-page');
      accTok.getString('access_token_sh') != null
          ? Navigator.pushReplacementNamed(context, '/main-page')
          : Navigator.pushReplacementNamed(context, '/login-page-one');
      // });
    } else {
      // First time
      prefs.setBool('first_time', false);
      Timer(_duration, () {
        Navigator.pushReplacementNamed(context, '/on-board-screen');
      });
      print('fisrt time');
      // return new Timer(_duration, navigationPageWel);
    }
  }

  // getInit() async {
  //   SharedPreferences accTok = await SharedPreferences.getInstance();
  //   await Provider.of<RapatProvider>(context, listen: false)
  //       .getRapat(accessToken: accTok.getString('access_token_sh').toString());
  //   Navigator.pushReplacementNamed(context, '/main-page');
  // }

  @override
  void initState() {
    // TODO: implement initState
    Timer(Duration(seconds: 3), () {
      checkStateInstall();
      // Navigator.pushReplacementNamed(context, '/on-board-screen');
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              CustomLogoImage(),
              Padding(
                padding: EdgeInsets.only(top: 20.0),
                child: Text(
                  "JSI APPS",
                  style: saphireTextStyle.copyWith(
                    fontSize: 18,
                    fontWeight: black,
                  ),
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}
