import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:jsi_apps/shared/custom_theme.dart';
import 'package:jsi_apps/widgets/custom_appbar_notitle.dart';
import 'package:jsi_apps/widgets/custom_background_pages.dart';
import 'package:jsi_apps/widgets/custom_button.dart';
//! DEPRECATED
class PresensiRapatEsignTwo extends StatelessWidget {
  const PresensiRapatEsignTwo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget buildContent() {
      return Padding(
        padding: EdgeInsets.only(
          left: defaultMargin,
          right: defaultMargin,
          top: defaultMargin,
          bottom: 100,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Rapat Akreditasi JSI 2021",
                  style: fiordTextStyle.copyWith(
                    fontSize: 15,
                    fontWeight: semibold,
                  ),
                ),
                SizedBox(
                  height: 11,
                ),
                Center(
                  //TODO: Masih bentuk Image
                  child: Container(
                    height: 300,
                    width: 333,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image:
                            AssetImage('assets/images/image_ttd_example.png'),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Icon(
                      Icons.image,
                      color: fiord,
                      size: 35,
                    ),
                    CustomButton(
                      title: 'Submit',
                      onPressed: () {
                        // Navigator.pushNamed(context, '/main-page');
                        Fluttertoast.showToast(
                            msg: "Submit",
                            toastLength: Toast.LENGTH_SHORT,
                            gravity: ToastGravity.BOTTOM,
                            timeInSecForIosWeb: 1,
                            backgroundColor: Colors.red,
                            textColor: Colors.white,
                            fontSize: 16.0);
                      },
                      width: 86,
                      bgColor: carrotOrange,
                    ),
                    CustomButton(
                      title: 'Cancel',
                      onPressed: () {
                        // Navigator.pushNamed(context, '/main-page');
                        Fluttertoast.showToast(
                            msg: "Cancel",
                            toastLength: Toast.LENGTH_SHORT,
                            gravity: ToastGravity.BOTTOM,
                            timeInSecForIosWeb: 1,
                            backgroundColor: Colors.red,
                            textColor: Colors.white,
                            fontSize: 16.0);
                      },
                      width: 86,
                      bgColor: fiord,
                    ),
                  ],
                )
              ],
            ),
            SizedBox(
              height: 16,
            ),
            // rapatList(),
          ],
        ),
      );
    }

    return Scaffold(
      body: Stack(
        children: [
          CustomBackgroundPage(),
          Column(
            children: [
              CustomAppbarNotitle(
                title: 'E-Sign',
              ), //! : Appbar masih bug,
              buildContent(),
            ],
          ),
        ],
      ),
    );
  }
}
