import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:jsi_apps/models/rapat_model.dart';
import 'package:jsi_apps/pages/presensi_rapat_esign_one.dart';
import 'package:jsi_apps/shared/custom_theme.dart';
import 'package:jsi_apps/widgets/custom_button.dart';

class RapatDetil extends StatefulWidget {
  final RapatModel rapat;
  const RapatDetil({Key? key, required this.rapat}) : super(key: key);

  @override
  State<RapatDetil> createState() => _RapatDetilState();
}

class _RapatDetilState extends State<RapatDetil> {
  @override
  Widget build(BuildContext context) {
    Widget backgroundHeader() {
      return Container(
        height: 300,
        width: double.infinity,
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('assets/images/bg_rapat_header.png'),
              fit: BoxFit.cover),
        ),
      );
    }

    Widget content() {
      return Container(
        padding: EdgeInsets.symmetric(vertical: 14),
        width: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ElevatedButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: Icon(
                Icons.arrow_back_rounded,
                color: Colors.black,
                size: 30.0,
              ),
              style: ElevatedButton.styleFrom(
                  shape: CircleBorder(), primary: Colors.white),
            ),
            SizedBox(
              height: 80,
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: defaultMargin),
              child: Text(
                widget.rapat.judulRapat.toString().toUpperCase(),
                style: whiteTextStyle.copyWith(
                    fontSize: 20, fontWeight: bold, letterSpacing: 3),
              ),
            ),
            Container(
              width: double.infinity,
              margin: EdgeInsets.only(top: 20),
              padding: EdgeInsets.symmetric(
                vertical: 14,
                horizontal: 10,
              ),
              decoration: BoxDecoration(
                color: kWhiteColor,
                borderRadius: BorderRadius.circular(40),
              ),
              child: Column(
                children: [
                  Text(
                    widget.rapat.unit.toString(),
                    style: blackTextStyle.copyWith(
                      fontSize: 15,
                      fontWeight: semibold,
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Tanggal & Waktu',
                              style: blackTextStyle.copyWith(
                                fontWeight: semibold,
                                fontSize: 12,
                              ),
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Row(
                              children: [
                                Text(
                                  // rapat.judulRapat!.toUpperCase(),
                                  widget.rapat.tanggal.toString(),
                                  style: mineShaftTextStyle.copyWith(
                                    fontWeight: reguler,
                                    fontSize: 11,
                                    letterSpacing: 1.5,
                                  ),
                                  textAlign: TextAlign.center,
                                  overflow: TextOverflow.ellipsis,
                                ),
                                SizedBox(
                                  width: 5,
                                ),
                                Text(
                                  // rapat.judulRapat!.toUpperCase(),
                                  '-',
                                  style: mineShaftTextStyle.copyWith(
                                    fontWeight: reguler,
                                    fontSize: 11,
                                  ),
                                  textAlign: TextAlign.center,
                                  overflow: TextOverflow.ellipsis,
                                ),
                                SizedBox(
                                  width: 5,
                                ),
                                Text(
                                  // rapat.judulRapat!.toUpperCase(),
                                  widget.rapat.waktu.toString(),
                                  style: mineShaftTextStyle.copyWith(
                                    fontWeight: reguler,
                                    fontSize: 11,
                                    letterSpacing: 1.5,
                                  ),
                                  textAlign: TextAlign.center,
                                  overflow: TextOverflow.ellipsis,
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                          top: 5,
                          bottom: 10,
                        ),
                        width: double.infinity,
                        height: 2,
                        decoration: BoxDecoration(
                          color: Color(0xffC0C0C0),
                          borderRadius: BorderRadius.circular(18),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Tempat',
                              style: blackTextStyle.copyWith(
                                  fontWeight: semibold,
                                  fontSize: 12,
                                  letterSpacing: 1.5),
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Text(
                              widget.rapat.tempat.toString(),
                              style: blackTextStyle.copyWith(
                                  fontSize: 11,
                                  fontWeight: reguler,
                                  letterSpacing: 3),
                              textAlign: TextAlign.justify,
                            ),
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                          top: 5,
                          bottom: 10,
                        ),
                        width: double.infinity,
                        height: 2,
                        decoration: BoxDecoration(
                          color: Color(0xffC0C0C0),
                          borderRadius: BorderRadius.circular(18),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Agenda',
                              style: blackTextStyle.copyWith(
                                  fontWeight: semibold,
                                  fontSize: 12,
                                  letterSpacing: 3),
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            // Text(
                            //   widget.rapat.agenda.toString(),
                            //   style: blackTextStyle.copyWith(
                            //       fontSize: 12,
                            //       fontWeight: semibold,
                            //       letterSpacing: 3),
                            //   textAlign: TextAlign.justify,
                            // ),
                            Html(data: widget.rapat.agenda)
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                          top: 5,
                          bottom: 10,
                        ),
                        width: double.infinity,
                        height: 2,
                        decoration: BoxDecoration(
                          color: Color(0xffC0C0C0),
                          borderRadius: BorderRadius.circular(18),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            )
          ],
        ),
      );
    }

    return SafeArea(
      child: Scaffold(
        body: Container(
          color: kWhiteColor,
          child: SingleChildScrollView(
            child: Column(
              children: [
                Stack(
                  children: [
                    backgroundHeader(),
                    content(),
                  ],
                ),
                CustomButton(
                  title: 'Presensi',
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => PresensiRapatEsignOne(
                          widget.rapat.judulRapat.toString(),
                          widget.rapat.id.toString(),
                        ),
                      ),
                    );
                  },
                  width: 86,
                  bgColor: carrotOrange,
                ),
                SizedBox(
                  height: 30,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
