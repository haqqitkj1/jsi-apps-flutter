import 'package:flutter/material.dart';
import 'package:jsi_apps/models/auth_model.dart';
import 'package:jsi_apps/models/rapat_model.dart';
import 'package:jsi_apps/providers/auth_provider.dart';
import 'package:jsi_apps/providers/rapat_provider.dart';
import 'package:jsi_apps/shared/custom_theme.dart';
import 'package:jsi_apps/widgets/custom_menu_page_item.dart';
import 'package:provider/provider.dart';

class MenuPage extends StatefulWidget {
  @override
  State<MenuPage> createState() => _MenuPageState();
}

class _MenuPageState extends State<MenuPage> {
  @override
  Widget build(BuildContext context) {
    // AuthProvider authProvider = Provider.of<AuthProvider>(context);
    // AuthModel authModel = authProvider.user;
    // print('${authModel.accessToken}');

    return Container(
      child: Column(children: [
        // CustomAppbarDefault(),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 21, vertical: 16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Aplikasi Saya",
                style: fiordTextStyle.copyWith(
                  fontSize: 15,
                  fontWeight: semibold,
                ),
              ),
              SizedBox(
                height: 16,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    CustomMenuPageItem(
                      isActive: true,
                      title: 'Presensi Rapat',
                      imageUrl: 'assets/images/ic_rapat.png',
                      onPressed: () {
                        Navigator.pushNamed(context, '/presensi-rapat-two');
                      },
                    ),
                    CustomMenuPageItem(
                      onPressed: () {},
                      isActive: true,
                      title: 'History Rapat',
                      imageUrl: 'assets/images/ic_rapat_history.png',
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 22,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    CustomMenuPageItem(
                      onPressed: () {},
                      isActive: false,
                    ),
                    CustomMenuPageItem(
                      onPressed: () {},
                      isActive: false,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ]),
    );
  }
}
