import 'package:flutter/material.dart';
import 'package:jsi_apps/shared/custom_theme.dart';
import 'package:jsi_apps/widgets/custom_button.dart';
import 'package:jsi_apps/widgets/custom_logo_image.dart';

class LoginPageOne extends StatelessWidget {
  const LoginPageOne({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget backgroundImage() {
      return Container(
        width: double.infinity,
        height: 613,
        decoration: BoxDecoration(
          image: DecorationImage(
            fit: BoxFit.fill,
            image: AssetImage('assets/images/bg_login1.png'),
          ),
        ),
      );
    }

    return Scaffold(
      backgroundColor: Color(0xffE7E2E2),
      body: Stack(
        children: [
          backgroundImage(),
          Container(
            margin: EdgeInsets.only(top: 200),
            child: Center(
              child: Column(
                children: [
                  CustomLogoImage(),
                  SizedBox(
                    height: 72,
                  ),
                  CustomButton(
                    title: 'Masuk Sebagai Pengunjung',
                    onPressed: () {},
                    width: 264,
                    bgColor: saphire,
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  CustomButton(
                    title: 'Masuk Dengan Akun Filkom',
                    onPressed: () {
                      Navigator.pushNamed(context, '/login-page-filkom');
                    },
                    width: 264,
                    bgColor: Color(0xffF29515),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
