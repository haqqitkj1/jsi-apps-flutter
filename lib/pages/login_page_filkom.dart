import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:jsi_apps/models/post_result_model.dart';
import 'package:jsi_apps/shared/custom_theme.dart';
import 'package:jsi_apps/widgets/custom_button.dart';

import 'package:http/http.dart' as http;

class LoginPageFilkom extends StatelessWidget {
  PostResult? postResult;
  LoginPageFilkom({Key? key, this.postResult}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    Widget backgroundImage() {
      return Container(
        width: double.infinity,
        height: double.infinity,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topRight,
            end: Alignment.bottomLeft,
            colors: <Color>[
              pancho,
              Color(0xFFA9B8D4),
            ],
          ),
        ),
      );
    }

    Widget loginInputSection(
      String title,
      String hint,
      bool obsecure,
    ) {
      return Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              title,
              style: fiordTextStyle.copyWith(
                fontSize: 14,
                fontWeight: semibold,
              ),
            ),
            SizedBox(
              height: 6,
            ),
            TextFormField(
              cursorColor: astronaut,
              obscureText: obsecure,
              style: fiordTextStyle.copyWith(color: kBlueDark),
              decoration: InputDecoration(
                filled: true,
                fillColor: kWhiteColor,
                contentPadding: EdgeInsets.fromLTRB(10, 10, 10, 0),
                hintText: hint,
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(defaultRadius5),
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(
                    defaultRadius5,
                  ),
                  borderSide: BorderSide(color: astronaut, width: 2),
                ),
              ),
            )
          ],
        ),
      );
    }

    return Scaffold(
      body: Stack(
        children: [
          backgroundImage(),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 35),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Sign In',
                  style: fiordTextStyle.copyWith(
                    fontSize: 16,
                    fontWeight: bold,
                  ),
                ),
                SizedBox(
                  height: 34,
                ),
                loginInputSection('Username', 'Your Username', false),
                SizedBox(
                  height: 21,
                ),
                loginInputSection('Password', 'Your Password', true),
                SizedBox(
                  height: 44,
                ),
                Center(
                  child: CustomButton(
                    title: 'Sign In',
                    onPressed: () {
                      PostResult.connectToAPI(
                              "6ad9e973117f59d8732be82a8238f022",
                              "205150709111005",
                              "1234")
                          .then((value) {
                        postResult = value;
                        // Navigator.of(context).pushNamedAndRemoveUntil(
                        //     '/main-page', (Route<dynamic> route) => false);
                        Fluttertoast.showToast(
                            msg: "Sign In Gan",
                            toastLength: Toast.LENGTH_SHORT,
                            gravity: ToastGravity.BOTTOM,
                            timeInSecForIosWeb: 1,
                            backgroundColor: Colors.red,
                            textColor: Colors.white,
                            fontSize: 16.0);
                      });
                    },
                    width: 86,
                    bgColor: carrotOrange,
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  // @override
  // State<StatefulWidget> createState() {
  //   // TODO: implement createState
  //   throw UnimplementedError();
  // }

  // CREATE FUNCTION TO CALL LOGIN POST API
  Future<void> login() async {
    await http.post(
      Uri.parse("https://apps-jsi.ub.ac.id/jsiapps/public/api/get-token"),
    );
  }
}
